# coding=utf-8
"""
Module for setting up emulation of the Nachi AX10 controller and SC15F
robot.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012-2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import threading

import site
import bge

from pymoco.simulator.nachi_olimex_emu import NachiOlimexEmu


def init():
    global olimex_emu, time_update
    import pymoco.robot
    basename = 'SC15F.Base'
    scene = bge.logic.getCurrentScene()
    base_go = None
    for o in scene.objects:
        if o.name.find(basename)>=0:
          base_go = o
          print('Found base object: "%s"'%base_go.name)
          break
    if not base_go is None:
        olimex_emu = NachiOlimexEmu(base_go,
                                    pymoco.robot.create_robot('NachiSC15F'),
                                    bind_host='127.0.0.1'
                                    #, angles_io=False
      )
        # '')
        time_update = olimex_emu.emit_state
        olimex_emu.start()
    else:
        print('Error: No base object found, emulator not started!')

shutdown_event = threading.Event()

def shutdown():
    global olimex_emu
    if not shutdown_event.isSet():
        shutdown_event.set()
        print('Shutting down ur emu.')
        olimex_emu.stop()
        olimex_emu.join()
        co = bge.logic.getCurrentController()
        actuator = co.actuators["exit"]
        co.activate(actuator)

