# coding=utf-8
"""
Module with facilities for emulation of robot controllers with the
Blender Game Engine.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012-2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import sys

import bge


obj_name = None


def _obj_name_24(obj_base):
    return 'OB' + obj_base


def _obj_name_26(obj_base):
    return obj_base


if sys.version_info[0] == 2:
    obj_name = _obj_name_24
if sys.version_info[0] == 3:
    obj_name = _obj_name_26


def is_rob_link(gO):
    """Determine if an object is a robot link, based on the 'Type'
    property."""
    return (type(gO) == bge.types.KX_GameObject and
            'Type' in gO and
            str(gO['Type']) in ['RobotBase', 'RobotLink', 'robot_base',
                                'robot_link'])


def find_armatured_links(base):
    """With 'base' object, find and sort all links that are
    children of a child armature of 'base'."""
    # Find armature and tool mount
    tool_mount = None
    arm = None
    for o in base.children:
        if type(o) == bge.types.BL_ArmatureObject:
            arm = o
        elif o.get('Type', '') in ['tool_mount_frame', 'ToolMountFrame',
                                   'tool_flange_frame']:
            print('Found tool mount {}'.format(o.name))
            tool_mount = o
    if arm is None:
        return None
    # Grab the links, which are children of the armature
    links = [base]+[o for o in arm.children
                    if o['Type'] in ['RobotLink', 'robot_link']]
    # Topological sort with respect to link number
    links.sort(key=lambda l: l['Link'])
    # Attached the tool mount to the last
    # link.
    if tool_mount is not None:
        print('Setting tool mount as child of last link ({})'
              .format(links[-1].name))
        tool_mount.removeParent()
        tool_mount.setParent(links[-1])
    return links


def find_link_chain(base):
    """From 'base' find a chain of links for a robot. The chain may
    either be based on children in a hierarchy, where breadth first
    search is applied. Or it may be based on all links being parented
    to an armature, which in turn is a child of 'base'."""
    frontier = [base]
    links = [base]
    # If first, check for armature, in which case all proper links are
    # children of that.
    if 'armatured' in base and base['armatured']:
        print('Armatured robot found.')
        return find_armatured_links(base)
    else:
        while frontier:  # Not empty
            o = frontier.pop(0)
            frontier += o.children
            links += filter(is_rob_link, o.children)
        return links
