# coding=utf-8
"""
Module for listening for joint configuration packages from the LLC
accessor, and publishing the information internally to subscribers.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import time
import traceback
import math
import struct
import threading
import socket
import types

import numpy as np

import pymoco.input
from . import  llcpublisher

PCK_ERR_TYPE = -1
SERVO_ANG_PCK_TYPE = 0
SERVO_ENC_PCK_TYPE = 1
MAIN_ENC_PCK_TYPE = 2

class NachiOlimexPublisher(llcpublisher.LLCPublisher):
    """ A class for publishing information from packages
    sent by the LLC to the main controller through the accessor."""

    class Error(Exception):
        """ Error class for thowing exceptions."""
        def __init__(self, message):
            Exception.__init__(self)
            self.message = message
        def __repr__(self):
            return self.__class__.__name__ + ' : ' + self.message

    # Buffer size for receiving data
    bufsz = 1024
    # The Olimex gateway sends three different types of joint
    # positions on the outgoing UDP-socket: Joint positions commanded
    # from the main controller, commanded from the Olimex interceptor,
    # and positions read off the encoders from the servo
    # controllers. The first two are bundled in one packet, and the
    # last is sent separate in it's own packet. It is the latter that
    # is to be grabbed and published. The joint positions
    servo_p_struct_angles = struct.Struct('<6d')
    servo_p_struct_encoders = struct.Struct('<6L')
    main_p_struct_encoders = struct.Struct('<12L')
    
    def __init__(self, **kwargs): 
        """Binding to 'host' try to read off controller packages from
        'olimex_out_port'."""
        llcpublisher.LLCPublisher.__init__(self, **kwargs)
        self._log_level = kwargs.get('log_level', 2)
        # Get the out port from the olimex. Use default from the input
        # module.
        self._olimex_host = kwargs.get('olimex_host', '127.0.0.1')
        self._olimex_out_port = kwargs.get('olimex_out_port',
                                           pymoco.input.llc_out_port)
        # The local network to bind to for listening.
        self._bind_host = kwargs.get('bind_host', '')
        if self._rob_def is None:
            raise self.Error('Must have valid robot '
                             'definition in argument "robotDef"!')
        # Cycle number counter
        self._rob_fac.cycle_number = -1
        # Get flag that choses whether angles or encoders are emitted
        # as joint positions by the Olimex application.
        self._angles_io = kwargs.get('angles_io', False)
        # Select appropriate struct for reading the joint positions.
        self._olimex_out_socket = socket.socket(socket.AF_INET,
                                                socket.SOCK_DGRAM)
        self._olimex_out_socket.setsockopt(socket.SOL_SOCKET,
                                           socket.SO_REUSEADDR, 1)
        self._olimex_out_socket.bind((self._bind_host, self._olimex_out_port))
        self._olimex_out_socket.settimeout(1.0)
        self._p_main_enc = np.zeros(self._rob_def.dof, dtype=np.double)
        # The host for the llc that we listen to.
        # self._olimex_host = kwargs.get('llc_host', None) 
        self._olimex_host = None
        # Default initial definition of configuration variables
        self._packet_recv_fails = 0
        self._last_p_time = time.time()
        self._initialize()
        threading.Thread.__init__(self,
                                  name='NachiOlimexPublisher <- %s:%d'
                                  %(self._olimex_host, self._olimex_out_port))
        self.daemon = True

    def get_llc_host(self):
        return self._olimex_host
    llc_host = property(get_llc_host)

    def _log(self, msg, log_level = 2):
        """ Message for filtered logging."""
        if log_level <= self._log_level:
            print(str(log_level) + ' : ' + self.__class__.__name__
                  +  ' (%s:%d)::' % (self._olimex_host,self._olimex_out_port)
                  + traceback.extract_stack(limit=2)[-2][2] + ' : ' + msg)
    
    def _initialize(self):
        """ Initialisation method for trying to establish a connection
        to the accessor."""
        self._log('Starting up on llc host %s, port %d' %
                  (self._olimex_host, self._olimex_out_port), 2)
        ## Try to receive a package on the server port
        self._initialized_event.clear()
        self._initializing = True
        # # New protocol does not broadcast asynchronously, hence commented!
        # try:
        #     init_packet = self._olimex_out_socket.recvfrom(self.bufsz)
        # except socket.timeout:
        #     self._log('Socket timed out', 2)
        #     return 
        # # Passed connection check, now initialize state data !!!
        # # Should verify contents or size !!!
        # self._olimex_host = init_packet[1][0]
        # self._log('Connected to %s' % self._olimex_host, 1)
        # Initialize configuration and time values by receiving two
        # appropriate packets (or packet sets).
        i = 2
        tries = 10
        if self._angles_io:
            i = 2
            while i > 0 and tries > 0:
                self._log('Trying to get Olimex servo packet: i,tries = ({},{})'
                          .format(i,tries), 3)
                if self._recv_olimex_packet():
                    recv_pck_type = self._parse_olimex_packet(initialize=True)
                    if recv_pck_type == SERVO_ANG_PCK_TYPE:
                        i -= 1
                tries -= 1
        else: 
            # Raw olimex packets
            i_servo = 2
            i_main = 1
            while i_main > 0 and tries > 0:
                self._log(
                    'Trying to get Olimex servo and main packet: '
                    + 'i,tries = ({},{})'.format(i,tries), 3)
                if self._recv_olimex_packet():
                    recv_pck_type = self._parse_olimex_packet(initialize=True)
                    if recv_pck_type == MAIN_ENC_PCK_TYPE:
                        i_main -= 1
                    elif recv_pck_type == SERVO_ENC_PCK_TYPE:
                        i_servo -=1
                tries -= 1            
        if tries > 0:
            # // Ensure that 0 velocity is assumed
            self._p_serial_vel = np.zeros(self._rob_def.dof)
            # // Ensure that the frame computer is initialized
            self._rob_fac.frame_computer(self._p_serial)
            self._initialized_event.set()
            self._log('Initialised OK', 2)
        else:
            self._initialized_event.clear()
            self._log('Initialised FAILED', 1)
        self._initializing = False
        return self.is_initialized
    
    def _parse_olimex_packet(self, initialize=False):
        """ Parse the current servo joint positions."""
        if self._p_data is None:
            return None
        with self._p_lock:
            if len(self._p_data) == self.servo_p_struct_encoders.size:
                # A new request and status from the servo
                self._rob_fac.cycle_number += 1
                servo_pos = np.array(
                    self.servo_p_struct_encoders.unpack(self._p_data))
                # // servo_pos is given in encoders' values.
                self._p_encoder_vel[:] = (
                    servo_pos - self._p_encoder) / self._period_est
                self._p_encoder[:] = servo_pos
                # // Discrete serial velocity estimate
                new_ser_pos = self._rob_def.encoder2serial(self._p_encoder)
                self._p_serial_vel[:] = (
                    new_ser_pos - self._p_serial_vel) / self._period_est
                self._p_serial[:] = new_ser_pos
                self._p_actuator[:] = self._rob_def.encoder2actuator(
                    self._p_encoder)
                return SERVO_ENC_PCK_TYPE
            elif len(self._p_data) == self.main_p_struct_encoders.size:
                # // The main controller position are the first six values
                self._p_encoders = self.main_p_struct_encoders.unpack(self._p_data)
                if initialize:
                    self._p_main_encoders = self._p_encoders[:6]
                self._p_servo_encoders = self._p_encoders[6:]
                servo_pos = self._p_servo_encoders
                self._p_encoder_vel[:] = (
                servo_pos - self._p_encoder) / self._period_est
                self._p_encoder[:] = servo_pos
                # // Discrete serial velocity estimate
                new_ser_pos = self._rob_def.encoder2serial(self._p_encoder)
                self._p_serial_vel[:] = (
                    new_ser_pos - self._p_serial_vel) / self._period_est
                self._p_serial[:] = new_ser_pos
                self._p_actuator[:] = self._rob_def.encoder2actuator(
                    self._p_encoder)
                return MAIN_ENC_PCK_TYPE
            else:
                return PCK_ERR_TYPE

    def _recv_packet(self):
        """ Private method for trying to receive a network packet from the
        connected Olimex application."""
        try:
            d = self._olimex_out_socket.recv(self.bufsz)
            dtime = time.time()
        except socket.timeout:
            # // Socket timout
            if self._log_level >= 2:
                self._log('Olimex socket timeout', 2)
            self._packet_recv_fails+=1
            return -1.0, ''
        except socket.error as emsg:
            # // Socket error
            if self._log_level >= 2:
                self._log('Olimex socket general error : %s' % str(emsg), 1)
            self._packet_recv_fails+=1
            return -1.0, ''
        else:
            self._packet_recv_fails=0
            return dtime, d

    def _recv_olimex_packet(self, tries=3):
        """ Receive an LLC to main controller package intercepted by
        the Olimex. The packet is matched by its size."""
        ## First test that we have a connection by receiving one package.
        dtime, d = self._recv_packet()
        if d == '':
            ## Assume no connection
            return False
        ## Assume we can get packages
        while tries > 0:
            if self._log_level >= 5:
                self._log('Received package of size %d'%len(d), 5)
            if self._angles_io and len(d) == self.servo_p_struct_angles.size:
                break
            elif (not self._angles_io 
                  and len(d) == self.main_p_struct_encoders.size):
                break
            elif (not self._angles_io 
                  and len(d) == self.servo_p_struct_encoders.size):
                break
            else:
                if self._log_level >= 5:
                    self._log('Wrong size. Expected %d, %d, or %d.'
                              % (self.servo_p_struct_angles.size
                                 , self.servo_p_struct_encoders.size
                                 , self.main_p_struct_encoders.size), 3)
            dtime, d = self._recv_packet()
            tries -= 1
        if tries == 0:
            return False
        with self._p_lock:
            self._p_data = d
            self._p_time = dtime
        return True
            
    def run(self):
        """ Main loop of the publisher: Get and process
        packages, notifying subscribers."""
        while not self._stop_flag.isSet() and self._packet_recv_fails < 10:
            if self._recv_olimex_packet():
                recv_pck_type = self._parse_olimex_packet()
                if recv_pck_type == MAIN_ENC_PCK_TYPE:
                    self._log('Notifying subscribers', 6)
                    self._notify_subscribers(self._p_time)
                    self._period_est = (0.1 * (self._p_time-self._last_p_time) 
                                        + 0.9 * self._period_est)
                    self._last_p_time = self._p_time
            else:
                if self._log_level >= 3:
                    self._log('Spurious package!!!!', 3)
        self._olimex_out_socket.close()
        self._log('Stopped.',1)
