# coding=utf-8
"""
Module for the robot control functionality for the joint jogger gui application. 
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012-2013"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import threading
import time
import sys
from cStringIO import StringIO
cvsio=StringIO()

import numpy as np

import pymoco
import pymoco.pymoco_client as pmc


class JointJoggerControl(threading.Thread):

    def __init__(self,  acc_max=2.0, target_speed=0.5):
        threading.Thread.__init__(self, name='JointJoggerControl')
        self.daemon = True
        self._jvc = None
        self._rob_type = None
        self._v_target = None
        self._p_target = None
        self._v_latest = np.zeros(6)
        self._acc_max = acc_max
        self._p_tol = 0.0001
        self._v_tol = 0.0001
        self._target_speed = target_speed
        self._going_to_rest = threading.Event()
        self._resting = threading.Event()
        self._control_lock = threading.Lock()
        self._llc_event = threading.Event()
        self._idle = True
        self._debug = False

    # def __getattr__(self, name):
    #     if name in ['acc_max', 'target_speed', 'cycle_time']:
    #         return eval('self._%s' % name)
    #     else:
    #         return  object.__getattr__(self, name)            

    def __setattr__(self, name, value):
        if name in ['acc_max', 'target_speed', 'cycle_time']:
            object.__setattr__(self, name, value)
        else:
            object.__setattr__(self, name, value)

        
    def set_robot(self, rob_type, llc_in_port=None, llc_out_port=None):
        if not self._rob_type is None:
            self.stop_control()
            with self._control_lock:
                self._jvc = None
                pmc.stop()
        if not rob_type == '<disabled>':
            if not llc_in_port is None:
                pymoco.control.llc_in_port = llc_in_port
            if not llc_out_port is None:
                pymoco.input.llc_out_port = llc_out_port
            pmc.setup(rob_type)
            self._rob_fac = pmc.robot_facade
            self._rob_fac.subscribe(self)
            pmc.startjvc()
            self._jvc = pmc.jvc
            self._rob_type = rob_type
        else:
            self._rob_type = None

    def get_pos_limits(self):
        return self._rob_fac.robot_definition.posLimAct
        
    def get_pos(self):
        return self._rob_fac.act_joint_pos

    def go_to_rest(self):
        self._going_to_rest.set()
        self._resting.clear()
        
    def wait_for_rest(self):
        self._resting.wait()
        
    def stop_control(self):
        self._v_target = None
        self._p_target = None
        if not self._jvc is None:
            self._resting.clear()
            self._going_to_rest.set()
            print('Waiting for rest')
            self._resting.wait()
        
    def position_control(self):
        #self.stop_control()
        self._p_target = self.get_pos()
        self._v_target = None
        
    def velocity_control(self):
        #self.stop_control()
        self._v_target = np.zeros(self._rob_fac.robot_definition.dof, dtype=float)
        self._p_target = None

    def set_vel_target(self, v_target):
        if not self._stop_flag and not self._v_target == None:
            self._resting.clear()
            if type(v_target) != np.ndarray:
                v_target = np.array(v_target)
            self._v_target = v_target
            return True
        else:
            return False
        
    def set_pos_target(self, p_target):
        self._resting.clear()
        if not self._stop_flag and not self._p_target == None:
            if type(p_target) != np.ndarray:
                p_target = np.array(p_target)
            self._p_target = p_target
            return True
        else:
            return False    
        
    def llc_notify(self, llcpkg):
        self._llc_event.set()

    def run(self):
        self._stop_flag = False
        while not self._stop_flag:
            tCycleStart = time.time()
            if not self._jvc is None:
                self._jvc.wait_for_control()
            else:
                self._resting.set()        
                time.sleep(0.1)
                continue
            with self._control_lock:
                if not self._jvc is None:
                    cycle_time = self._rob_fac.control_period
                    dvMax = self._acc_max * cycle_time
                    curVel = self._jvc.cmd_velocity
                    if self._going_to_rest.isSet():
                        print('Going to rest')
                        dv = -curVel
                        dv = dv.clip(-dvMax, dvMax)
                        vNew = curVel+dv
                        if   np.max(np.abs(vNew)) <  self._v_tol:
                            print('Setting rest')
                            vNew = np.zeros(6)
                            self._resting.set()
                            self._going_to_rest.clear()
                            if not self._p_target is None:
                                self._p_target = self._rob_fac.act_joint_pos
                    elif not self._v_target is None:
                        dv = self._v_target - curVel
                        dv = dv.clip(-dvMax, dvMax)
                        vNew = curVel+dv
                    elif not self._p_target is None:
                        dv = self._pDvStep(curVel, cycle_time, dvMax)
                        vNew = curVel+dv
                    else:
                        print('Warning: not going to rest, no v-target, and no p-target!')
                        vNew = curVel
                    self._jvc.cmd_velocity = vNew
                    self._v_latest = vNew
                    if np.linalg.norm(self._jvc.cmd_velocity) < 0.0001:
                        self._resting.set()
                    

    def _brakeLength(self, v, dvMax, tC):
        ## Compute the braking distance
        fullCycles = np.floor(v / dvMax)
        remV = v - fullCycles * dvMax
        return (fullCycles * v - dvMax *(fullCycles+1) * fullCycles / 2.0 + remV) * tC
    
    def _pDvStep(self, curVel, tC, dvMax):
        ## Estimate the position we have commanded, and are supposed
        ## to achieve at the end of this cycle
        cmdPos =self._rob_fac.cmd_joint_pos ## Cheating
        ## Extrapolation does not work:
        # lastPos = self.get_pos()
        # cmdPos = lastPos +  curVel * tC ## Extrapolation
        ## The delta vector to the target from the next destination.
        delta_target = self._p_target - cmdPos
        ## Array of directions to the target from the cycle destination.
        dir_target = np.sign(delta_target)
        ## Vector to hold the delta velocities to return
        dv = np.zeros(len(curVel), float)
        if self._debug: dvmsg = ''
        for i in range(len(curVel)):
            if self._debug: dvmsg+=' '
            cv = curVel[i]
            acv = np.abs(cv)
            dirtgt = dir_target[i]
            dtgt = delta_target[i]
            adtgt = np.abs(dtgt)
            bdist = self._brakeLength(acv, dvMax, tC)
            ## Test if we are within tolerance and may stop in next cycle:
            if adtgt <= self._p_tol and acv <= dvMax:
                if self._debug: dvmsg += 's0'
                dv[i] = -cv
                continue
            ## Test if target may be reached in one speed change
            vOneStep = dtgt/tC
            dvOneStep = vOneStep - cv
            if np.abs(dvOneStep) <= dvMax and np.abs(vOneStep)<=dvMax:
                if self._debug: dvmsg += 's1'
                dv[i] = dvOneStep
                continue
            ## If moving in the right direction
            if dirtgt == np.sign(cv) and dirtgt != 0:
                ## Test if we are late for braking
                if adtgt < bdist:
                    if self._debug: dvmsg += 'be'
                    dv[i] =  - dvMax * dirtgt
                    continue
                ## Test if it is time for regular braking
                elif adtgt-acv*tC < bdist:
                    if self._debug: dvmsg += 'br'
                    dv[i] =  - dvMax * dirtgt
                    continue
            else:
                ## We are moving in the wrong direction or standing
                ## still. Give a kick in the right direction.
                if self._debug: dvmsg += 'w '
                dv[i] = dvMax * dirtgt
                continue
            ## Not stopping and moving in the right direction. Adjust
            ## towards target speed. (This must be possible to do
            ## in a simpler way!)
            if (adtgt - (acv + dvMax) * tC) > bdist:
                if self._debug: dvmsg += 'r'
                if dirtgt < 0.0:
                    vt= - self._target_speed
                    if cv > vt:
                        if self._debug: dvmsg += '1'
                        dv[i] = -min(dvMax,  cv - vt)
                    else:
                        if self._debug: dvmsg += '2'
                        dv[i] = min(dvMax, vt - cv)
                elif dirtgt > 0.0:
                    vt = self._target_speed
                    if cv < vt:
                        if self._debug: dvmsg += '3'
                        dv[i] = min(dvMax, vt - cv)
                    else:
                        if self._debug: dvmsg += '4'
                        dv[i] = -min(dvMax, cv - vt)
                continue
            if self._debug: dvmsg += 'u '
        if self._debug:
            cvsio.seek(0)
            cvsio.truncate()
            np.savetxt(cvsio, curVel, fmt='%6.3f', newline=',')
            print('%s [%s]'%(dvmsg,cvsio.getvalue()))
        return dv
    
    def stop(self):
        self.stop_control()
        self._stop_flag = True

##Test:
"""
if __name__ == '__main__':
import joint_jogger_control
jjc=joint_jogger_control.JointJoggerControl()
jjc.start()
jjc.set_robot('ur')
jjc.position_control()
p=jjc.get_pos()
p[0]=0.5
jjc.set_pos_target(p)
import atexit
atexit.register(jjc.stop)
#import code
#code.interact(None,None,globals())
"""
