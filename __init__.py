# coding=utf-8

"""
Initialization module for the PyMoCo top level package.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2014"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import sys
import os

# Load plugins

import pymoco.robot
import pymoco.simulator
import pymoco.control.facilities
import pymoco.facades
import pymoco.input

# Packages in the plugin which will be merged into pymoco packages.
pkg_name_to_pkg = {
    'robot': pymoco.robot,
    'simulator': pymoco.simulator,
    'facades': pymoco.facades
    }

# The name of the initializer script, if necessary, which should be provided by the plugin in the plugin directory.
__plugin_initializer_name = '__initialize_pymoco_plugin__.py'

_pymoco_dir = os.path.dirname(__file__)
_plugins_dir = os.path.join(_pymoco_dir, 'plugins')

for plugin_name in os.listdir(_plugins_dir):
    # Search and load recognized packages
    pdir = os.path.join(_plugins_dir, plugin_name)
    if os.path.isdir(pdir):
        for plpkg in os.listdir(pdir):
            if plpkg in pkg_name_to_pkg:
                pkg_name_to_pkg[plpkg].__path__.extend([os.path.join(pdir,plpkg)])
        # Execute the initializer
        if __plugin_initializer_name in os.listdir(pdir):
            exec(open(os.path.join(pdir, __plugin_initializer_name)).read())
