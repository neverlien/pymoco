# coding=utf-8
"""
Package for definition and provision of standard robot system facades.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2013"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

from .robot_facade import RobotFacade
from .ur_udp_legacy import URUDPLegacyFacade
from .ur_tcp_legacy import URTCPLegacyFacade
from .nachi_olimex_legacy import NachiOlimexLegacyFacade
