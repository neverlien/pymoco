# coding=utf-8
"""
Module for a legacy facade to a robot controller. A legacy facade is a
facade to the mechanisms that utilize separate control and input
connections in relation to the robot controller. Specifically the
control facilities and the LLC publishers in PyMoCo.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2013"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import traceback

from .robot_facade import RobotFacade

class LegacyFacade(RobotFacade):
    """ The legacy facades are facades to the legacy mechanisms of
    separate LLC publishers for receiving robot controller status and
    motion controller base class for sending motion commands to the
    robot controller."""
    def __init__(self, **kwargs):
        RobotFacade.__init__(self, **kwargs)
        self._scale_commands = kwargs.get('scale_commands',True)

    def _log(self, msg, log_level=2):
        if log_level <= self._log_level:
            print(str(log_level) + ' : ' + self.__class__.__name__ \
                  + '::' + traceback.extract_stack()[-2][2] + ' : ' + msg)

    def get_control_period(self):
        return self._llcp._period_est
    control_period = property(get_control_period)

    def get_current_arrival_time(self):
        return self._llcp._p_time
    current_arrival_time = property(get_current_arrival_time)

    # def get_q_zero_offsets(self):
    #     return self._q_zero_offsets

    def get_act_joint_vel(self):
        return self._llcp.serial_joint_velocity
    act_joint_vel = property(get_act_joint_vel)

    def get_act_joint_pos(self):
        return self._llcp.serial_joint_position
    act_joint_pos = property(get_act_joint_pos)

    def get_cmd_joint_pos(self):
        return self._control._q_tracked
    def set_cmd_joint_pos(self, joint_pos):
        self._control._set_serial_config(
            joint_pos, do_scale=self._scale_commands)
        with self._control_cond:
            self._control_cond.notify_all()
    cmd_joint_pos = property(get_cmd_joint_pos, set_cmd_joint_pos)

    def get_cmd_joint_increment(self):
        return self._control._q_increment
    def set_cmd_joint_increment(self, joint_inc):
        self._control._set_serial_increment(
            joint_inc, do_scale=self._scale_commands)
        with self._control_cond:
            self._control_cond.notify_all()
    cmd_joint_increment = property(get_cmd_joint_increment,
                                   set_cmd_joint_increment)

    def wait_for_initialized(self):
        self._llcp.wait_for_initialized()
        self._control.wait_for_initialized()

    def wait_for_control_initialized(self):
        self._control.wait_for_initialized()

    def wait_for_connection_initialized(self):
        self._llcp.wait_for_initialized()

    def start(self):
        raise NotImplementedError()

    def stop(self, join=False):
        raise NotImplementedError()
