# coding=utf-8

"""
Module for defining the base class and interface for all robot system facades.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2018"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import traceback
import threading

from .event_publisher import EventPublisher


class RobotFacade(object):
    
    joint_spaces = ['actuator', 'serial', 'encoder']

    def __init__(self, **kwargs):
        joint_space = kwargs.get('joint_space', 'serial')
        if joint_space in RobotFacade.joint_spaces:
            self._joint_space = joint_space
        else:
            raise Exception('Tried to set unknown joint_space: "{}"'
                            .format(joint_space))
        self._event_publisher = EventPublisher()
        self._control_cond = threading.Condition()

    def wait_for_control(self, timeout=None):
        """Wait for an active controller to completed a control cycle
        towards the robot system. This happens when a new joint
        position target or increment has been set.
        """
        with self._control_cond:
            self._control_cond.wait(timeout)

    def subscribe(self, subscriber):
        """Subscribe to real-time events from the robot
        system. 'subscriber' must be a callable taking one
        parameter. When called, the argument to 'subscriber' will be
        the earliest known receive time of the event.
        """
        self._event_publisher.subscribe(subscriber)

    def unsubscribe(self, subscriber):
        """Remove the 'subscriber' from listening for events."""
        self._event_publisher.unsubscribe(subscriber)

    def wait_for_event(self, timeout=None):
        """Wait for the next real-time event from the robot
        system.
        """
        self._event_publisher.wait_for_event(timeout)

    def get_robot_definition(self):
        """Return the robot definition in use by the facade. This is the
        access to the kinematics and configuration.
        """
        return self._rob_def

    robot_definition = property(get_robot_definition)

    def get_frame_computer(self):
        """Return the frame computer in use by the facade. This is the access
        to the kinematics and current configuration.
        """
        return self._frame_computer

    frame_computer = property(get_frame_computer)

    def get_joint_space(self):
        return self._joint_space

    joint_space = property(get_joint_space)

    def get_control_period(self):
        raise NotImplementedError(
            '"' + traceback.extract_stack(limit=1)[-1][2] + '"'
            + ' not implemented in abstract base class "RobotFacade"')

    control_period = property(get_control_period)

    def get_current_arrival_time(self):
        """Return the time at which the current, i.e. latest, status
        packet was received from the robot controller."""
        raise NotImplementedError(
            '"' + traceback.extract_stack(limit=1)[-1][2] + '"'
            + ' not implemented in abstract base class "RobotFacade"')

    current_arrival_time = property(get_current_arrival_time)

    def get_q_zero_offsets(self):
        """Return the actual zero offset used for the specific robot
        controlled."""
        raise NotImplementedError(
            '"' + traceback.extract_stack(limit=1)[-1][2] + '"'
            + ' not implemented in abstract base class "RobotFacade"')

    q_zero_offsets = property(get_q_zero_offsets)

    def get_cmd_joint_pos(self):
        """Get the latest commanded joint positions."""
        raise NotImplementedError(
            '"' + traceback.extract_stack(limit=1)[-1][2] + '"'
            + ' not implemented in abstract base class "RobotFacade"')

    def set_cmd_joint_pos(self, joint_pos):
        """Set the commanded joint positions."""
        raise NotImplementedError(
            '"' + traceback.extract_stack(limit=1)[-1][2] + '"'
            + ' not implemented in abstract base class "RobotFacade"')

    cmd_joint_pos = property(get_cmd_joint_pos, set_cmd_joint_pos)

    def get_act_joint_vel(self):
        """Get the actual joint velocity as reported from the robot system, or
        inferred from the history actual positions.
        """
        raise NotImplementedError(
            '"' + traceback.extract_stack(limit=1)[-1][2] + '"'
            + ' not implemented in abstract base class "RobotFacade"')

    act_joint_vel = property(get_act_joint_vel)

    def get_act_joint_pos(self):
        """Get the actual joint positions as reported from the robot
        system.
        """
        raise NotImplementedError(
            '"' + traceback.extract_stack(limit=1)[-1][2] + '"'
            + ' not implemented in abstract base class "RobotFacade"')

    act_joint_pos = property(get_act_joint_pos)

    def get_cmd_joint_increment(self):
        """Get the latest commanded increment in joint positions."""
        raise NotImplementedError(
            '"' + traceback.extract_stack(limit=1)[-1][2] + '"'
            + ' not implemented in abstract base class "RobotFacade"')

    def set_cmd_joint_increment(self, joint_inc):
        """Command an increment in joint positions."""
        raise NotImplementedError(
            '"' + traceback.extract_stack(limit=1)[-1][2] + '"'
            + ' not implemented in abstract base class "RobotFacade"')

    cmd_joint_increment = property(get_cmd_joint_increment,
                                   set_cmd_joint_increment)

    def stop(self, join=False):
        """Stop the interaction with the robot system."""
        raise NotImplementedError(
            '"' + traceback.extract_stack(limit=1)[-1][2] + '"'
            + ' not implemented in abstract base class "RobotFacade"')
