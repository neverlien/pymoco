# coding=utf-8
"""
Module with definitions for the Universal Robots robots.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import struct

import numpy as np
import math3d as m3d

#from ..pymoco
from pymoco.kinematics import joints
from .robotdefinition import RobotDefinition

class UR_Base(RobotDefinition):

    def __init__(self, **kwargs):
        self._dof = 6
        RobotDefinition.__init__(self, **kwargs)
        # Gains in encoder count per radian of actuator joint
        # configuration values: qenc = gain*qact+offset. Gain and
        # offset vector will depend on the calibration and of the
        # robot joint mechanics!
        self._nominal_encoder_gains = np.ones(self._dof, dtype=np.float64)
        self._encoder_gains =  kwargs.get(
            'encoder_gains', self._nominal_encoder_gains.copy())
        self._nominal_encoder_offsets = np.zeros(self._dof, dtype=np.float64)
        self.encoder_offsets = kwargs.get(
            'encoder_offsets', self._nominal_encoder_offsets.copy())
        # Robot home for posing like a regular industrial robot
        self._q_home = np.array([0.0, -np.pi/2, - np.pi / 2,
                                 -np.pi / 2, np.pi / 2, 0.0])


    def set_encoder_offsets(self, new_encoder_offsets):
        self._encoder_offsets = new_encoder_offsets
        # Joint position limits
        self._pos_lim_act = 2.0 * np.pi * np.ones((2, self._dof))
        self._pos_lim_act[0] *= -1.0
        self._pos_lim_enc = (self._encoder_gains
                             * self._pos_lim_act + self._encoder_offsets)
        self._pos_lim_enc.sort(axis=0)
        # Joint speed limits
        self._spd_lim_act = np.pi * np.ones(self._dof)
        self._spd_lim_enc = np.abs(self._encoder_gains) * self._spd_lim_act
        # Joint acceleration limits
        self._acc_lim_act = np.append(25.0 * np.ones(3), 15.0 * np.ones(3))
        self._acc_lim_enc = np.abs(self._encoder_gains) * self._acc_lim_act
    encoder_offsets = property(RobotDefinition.get_encoder_offsets,
                               set_encoder_offsets)

    def get_joint_xforms(self):
        return [joints.RevoluteJoint() for i in range(self._dof)]
    joint_xforms = property(get_joint_xforms)

    def actuator2encoder(self, qact):
        """ Compute the encoder vector given a actuator"""
        # Really gains * qact + offsets
        return qact + self._encoder_offsets

    def encoder2actuator(self, qenc):
        """ Compute the actuator kinematics joint configuration from
        the encoder vector."""
        # Really (qenc - offsets) / gains
        return qenc - self._encoder_offsets

    def actuator2serial(self, qact):
        """ Convert serial kinematics configuration to actuator
        (actuatoruator joint) kinematics. Trivial due to identity of
        actuator and serial kinematics."""
        return qact

    def serial2actuator(self, qser):
        """ Convert serial kinematics configuration to actuator
        kinematics. Trivial due to identity of actuator and serial
        kinematics."""
        return qser

    def serial2encoder(self, qser):
        """ Convert serial joint configuration to encoder vector."""
        # Really gains * qser + offsets
        return qser + self._encoder_offsets

    def encoder2serial(self, qenc):
        """ Convert a encoder vector to serial joint configuration."""
        # Really (qenc - offsets) / gains
        return qenc - self._encoder_offsets
