# coding=utf-8
"""
Module with definitions for the Nachi SC15F robot.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2013"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import numpy as np
import math3d as m3d

from pymoco.kinematics import joints
from .robotdefinition import RobotDefinition

class NachiSC15F(RobotDefinition):

    def __init__(self, **kwargs):
        self._dof = 6
        RobotDefinition.__init__(self, **kwargs)

        # // gains in encoder units per radian of actuator joint configuration values:
        # // qEnc = gain*qAct+offset. Gain and offset vector will depend on the
        # // calibration of the robot!
        self._nominal_encoder_gains = np.array(
            [-26076.0, 26076.0, -26076.0, 14859.5, 14859.5, 8668.0],
            dtype=np.float64)
        self._encoder_gains =  kwargs.get(
            'encoder_gains', self._nominal_encoder_gains.copy())
        self._nominal_encoder_offsets = np.array(
            [0x80000, 0x80000, 0x80000, 0x80000, 0x80000, 0x80000],
            #[0x7a082, 0x8009b, 0x7fa6b, 0x80298, 0x7ffe8, 0x80c08],
            dtype=np.uint64)
        self.encoder_offsets = kwargs.get('encoder_offsets', 
                                          self._nominal_encoder_offsets)
        # // Home pose
        self._q_home = np.zeros(6, dtype=np.float64)


        # // Internal link transform for the seven robot parts.
        self._link_xforms = [m3d.Transform() for i in range(7)]

        self._link_xforms[0]._v.z = 0.115

        self._link_xforms[1]._o.rotateX(np.pi / 2)
        self._link_xforms[1]._v.x = 0.25
        self._link_xforms[1]._v.z = 0.55 - self._link_xforms[0]._v.z

        self._link_xforms[2]._v.y = 0.6

        self._link_xforms[3]._o.rotateY(np.pi / 2)
        self._link_xforms[3]._v.x = 0.12
        self._link_xforms[3]._v.y = 0.12

        self._link_xforms[4]._o.rotateY(-np.pi / 2)
        self._link_xforms[4]._v.z = 0.75 - self._link_xforms[3]._v.x

        self._link_xforms[5]._o.rotateY(np.pi / 2)
        self._link_xforms[5]._v.x = 0.10

        # // Notice reverse order of rotations(?)
        #_link_xforms[6]._o.rotateXB(-np.pi / 2)
        #_link_xforms[6]._o.rotateYB(-np.pi / 2)
        self._link_xforms[6]._v.z = 0.14 - self._link_xforms[5]._v.x

        # // Joint coupling transforms
        self._act2ser_coupling = np.identity(6)
        # // The parallel linkage
        self._act2ser_coupling[2,1] = -1.0
        # // The coupling from the lower arm roll to wrist pitch and tool roll
        self._act2ser_coupling[4,3] = 0.02
        self._act2ser_coupling[5,3] = 0.03497
        self._act2ser_coupling[5,4] = -0.03497
        # // Serial to actuator
        self._ser2act_coupling = np.linalg.inv(self._act2ser_coupling)

    def get_encoder_offsets(self):
        return self._encoder_offsets
    def set_encoder_offsets(self, new_encoder_offsets):
        self._encoder_offsets = new_encoder_offsets.copy()
        # // Joint position limits. May be asymmetric.
        self._pos_lim_act = np.pi/180.0 * np.array(
            [[-160, -90, -140, -270, -125, -450],
            [160, 150, 120, 270, 125, 450]], dtype=np.float64)
        self._pos_lim_enc = self.actuator2encoder(self._pos_lim_act)
        self._pos_lim_enc.sort(axis=0)
        # pos_lim_actLower, pos_lim_actUpper = pos_lim_act
        # pos_lim_encLower, pos_lim_encUpper = pos_lim_enc

        # // Joint speed limits. Symmetric
        self._spd_lim_act = np.pi / 180.0 * np.array(
            [130.0, 130.0, 130.0, 263.0, 263.0, 450.0], dtype=np.float64)
        self._spd_lim_enc = (abs(self._encoder_gains) *
                             self._spd_lim_act).astype(np.uint64)
    encoder_offsets = property(get_encoder_offsets, set_encoder_offsets)


    def get_joint_xforms(self):
        return [joints.RevoluteJoint() for i in range(6)]

    def actuator2encoder(self, qact):
        """Compute vector of encoder positions given a actuator"""
        return ((self._encoder_gains * qact).astype(np.int64)
                + self._encoder_offsets)

    def encoder2actuator(self, qenc):
        """Compute the actuator kinematics joint configuration from
        the encoder vector.""" 
        return (qenc - self._encoder_offsets) / self._encoder_gains

    def actuator2serial(self, qact):
        """Convert serial kinematics configuration to actuator (actuator joint)
        kinematics."""
        qser = qact.copy()
        qser = np.dot(self._act2ser_coupling, qact)
        return qser

    def serial2actuator(self, qser):
        """Convert serial kinematics configuration to actuator
        kinematics."""
        qact = qser.copy()
        qact = np.dot(self._ser2act_coupling, qser)
        return qact

    def serial2encoder(self, qser):
        """Convert serial joint configuration to encoder vector."""
        qact = qser.copy()
        qact = np.dot(self._ser2act_coupling, qser)
        return ((self._encoder_gains * qact).astype(np.int64) +
                self._encoder_offsets)

    def encoder2serial(self, qenc):
        """Convert a encoder vector to serial joint configuration."""
        qact = (qenc - self._encoder_offsets) / self._encoder_gains
        qser = qact.copy()
        qser = np.dot(self._act2ser_coupling, qact)
        return qser
