# coding=utf-8
"""
Module with definitions for the Universal Robots UR5 with
implementation of special ML-kinematics.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2013"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import struct

import numpy as np
import math3d as m3d

#from ..pymoco
from pymoco.kinematics import joints
from .ur5_ml import UR5_ML

class UR5_ML_CylRolls(UR5_ML):

    def __init__(self, **kwargs):
        UR5_ML.__init__(self, **kwargs)

        ## Internal link transform for the seven robot parts.

        # // Get and set the cylinder rolls
        self._cylinder_rolls = np.empty(2, dtype=np.float64)
        self._cylinder_roll_lines = np.empty((2,2), dtype=np.object) 
        self.cylinder_rolls = kwargs.get('cylinder_rolls', 
                                         np.zeros(2, dtype=np.float64))

    def get_inner_cylinder_line(self):
        """The inner roll line is defined with an offset line in the link 2
        inward coordinate system."""
        return self._cylinder_roll_lines[0]
    inner_cylinder_line = property(get_inner_cylinder_line)

    def get_inner_cylinder_roll(self):
        return self._cylinder_rolls[0]
    def set_inner_cylinder_roll(self, icr):
        """The inner roll is defined with an offset line in the link 2
        inward coordinate system."""
        self._cylinder_rolls[0] = icr
        c_offz0 = 0.06780
        c_offz1 = - c_offz0
        lx2 = m3d.Transform()
        lx2.pos.z += c_offz0
        lx2.orient.rotate_xt(-icr)
        self._cylinder_roll_lines[0] = (lx2.pos.copy(), 
                                        -lx2.orient.vec_x.copy())
        lx2.pos.x += -0.425
        lx2.pos += c_offz1 * lx2.orient.vec_z
        self._link_xforms[2] = lx2
    inner_cylinder_roll = property(get_inner_cylinder_roll,
                                   set_inner_cylinder_roll)
    
    def get_outer_cylinder_line(self):
        """The outer roll line is defined with an offset line in the link 3
        inward coordinate system."""
        return self._cylinder_roll_lines[1]
    outer_cylinder_line = property(get_outer_cylinder_line)

    def get_outer_cylinder_roll(self):
        return self._cylinder_rolls[1]
    def set_outer_cylinder_roll(self, ocr):
        """The outer roll is defined with an offset line in the link 3
        inward coordinate system."""
        self._cylinder_rolls[1] = ocr
        c_offz0 = - 0.04601
        c_offz1 = - c_offz0 + 0.002750
        lx3 = m3d.Transform()
        lx3.pos.z += c_offz0
        lx3.orient.rotate_xt(-ocr)
        self._cylinder_roll_lines[1] = (lx3.pos.copy(), -lx3.orient.vec_x.copy())
        lx3.pos.x += -0.39243
        lx3.pos += c_offz1 * lx3.orient.vec_z
        self._link_xforms[3] = lx3
    outer_cylinder_roll = property(get_outer_cylinder_roll,
                                   set_outer_cylinder_roll)
        
    def get_cylinder_roll_lines(self):
        """The cylinder roll lines are defined in their inward link
        coordinate systems. Hence, with respect to the frame computer,
        the inner cylinder roll line (at index 0) is defined in
        fc.inframe[2] and the outer cylinder roll line (at index 1) is
        definede in fc.inframe[3]."""
        return self._cylinder_roll_lines
    cylinder_roll_lines = property(get_cylinder_roll_lines)

    def get_cylinder_rolls(self):
        return self._cylinder_rolls
    def set_cylinder_rolls(self, rolls):
        self.inner_cylinder_roll = rolls[0]
        self.outer_cylinder_roll = rolls[1]
    cylinder_rolls = property(get_cylinder_rolls, set_cylinder_rolls)
