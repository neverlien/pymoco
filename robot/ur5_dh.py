# coding=utf-8
"""
Module with definitions for the Universal Robots UR5 with
implementation of DH-kinematics.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import struct

import numpy as np
import math3d as m3d

from pymoco.kinematics import joints
from .ur_base import UR_Base

class UR5_DH(UR_Base):

    def __init__(self, **kwargs):
        UR_Base.__init__(self, **kwargs)

        # Nominal DH-parameters from controller:
        self._as = [0.00000, -0.42500, -0.39243,  0.00000,  0.00000,  0.0000]
        self._ds = [0.08920,  0.00000,  0.00000,  0.10900,  0.09300,  0.0820]  
        self._alphas = [ 1.570796327, 0, 0, 1.570796327, -1.570796327, 0 ]

        # Internal link transform for the seven robot parts.
        # All link transforms are inherently packed in the dh-joint transforms
        self._link_xforms = [None] * 7
        # Exceptions are the base link transform and the tool flange transform (?)
        self._link_xforms[0] = m3d.Transform()
        self._link_xforms[0]._v.z = 0.021
        # _link_xforms[6] = m3d.Transform()
        # _link_xforms[6]._v.z = 0.032785

    def get_joint_xforms(self):
        return [joints.DHRevJoint(*pars) for pars in 
                zip(self._as, self._ds, self._alphas)]
    joint_xforms = property(get_joint_xforms)
