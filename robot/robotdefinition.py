# coding=utf-8
"""
Module with base class for defining robots.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2013; SINTEF 2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import pymoco.kinematics


class RobotDefinition(object):
    """Abstract (interface) base class for robot definition
    objects. Basic methods implemented here depend on access to data
    defined in sub-classes. """

    def __init__(self, **kwargs):

        # gains in encoder units per radian of actuator joint
        # configuration values: qEnc = gain*qAct+offset. Gain and
        # offset vector will depend on the calibration of the robot!
        # Format: numpy.array, dtype=numpy.double, shape=(N,)
        self._encoder_gains = None
        # Format: numpy.array dtype=numpy.long, shape=(N,)
        self._encoder_offsets = None
        # Format: numpy.array, dtype=numpy.double, shape=(N,)
        self._nominal_encoder_gains = None
        # Format: numpy.array dtype=numpy.long, shape=(N,)
        self._nominal_encoder_offsets = None

        # Joint position limits. May be asymmetric: Lower and upper
        # limits for joint positions.
        # Format: numpy.array, dtype=numpy.double, shape=(2,N)
        self._pos_lim_act = None
        # Format: numpy.array, dtype=numpy.long, shape=(2,N)
        self._pos_lim_enc = None

        # Joint speed limits. Symmetric.
        # Format: numpy.array, dtype=numpy.double, shape=(N,)
        self._spd_lim_act = None
        # Format: numpy.array dtype=numpy.long, shape=(N,)
        self._spd_lim_enc = None

        # Joint acceleration limits
        self._acc_lim_act = None
        self._acc_lim_enc = None

        # Home joint pose of the specific robot
        self._q_home = None

        # An optional placeholder for a kinematics computer
        # (FrameComputer) for use with special operations. If special
        # operations are used, without a kinematics computer has been
        # given, the operation fails with an exception.
        self._frame_comp = None

    def _notify_kin_update(self):
        """Notification to all frame computers that the kinematic
        parameters have been updated.
        """
        if self._frame_comp is not None:
            self._frame_comp.refresh_link_xforms()

    def get_pos_lim_act(self):
        return self._pos_lim_act

    pos_lim_act = property(get_pos_lim_act)

    def get_pos_lim_enc(self):
        return self._pos_lim_enc

    pos_lim_enc = property(get_pos_lim_enc)

    def get_spd_lim_act(self):
        return self._spd_lim_act

    spd_lim_act = property(get_spd_lim_act)

    def get_spd_lim_enc(self):
        return self._spd_lim_enc

    spd_lim_enc = property(get_spd_lim_enc)

    def get_acc_lim_act(self):
        return self._acc_lim_act

    acc_lim_act = property(get_acc_lim_act)

    def get_acc_lim_enc(self):
        return self._acc_lim_enc

    acc_lim_enc = property(get_acc_lim_enc)

    def get_q_home(self):
        return self._q_home

    q_home = property(get_q_home)

    def get_encoder_offsets(self):
        return self._encoder_offsets

    def set_encoder_offsets(self, new_encoder_offsets):
        raise NotImplementedError(
            'Must be overridden in specialized RobotDefinition')

    encoder_offsets = property(get_encoder_offsets, set_encoder_offsets)

    def get_nominal_encoder_offsets(self):
        return self._nominal_encoder_offsets

    nominal_encoder_offsets = property(get_nominal_encoder_offsets)

    def get_encoder_gains(self):
        return self._encoder_gains

    encoder_gains = property(get_encoder_gains)

    def get_nominal_encoder_gains(self):
        return self._nominal_encoder_gains

    nominal_encoder_gains = property(get_nominal_encoder_gains)

    def get_kinematic_parameters(self):
        raise NotImplementedError(
            'Must be overridden in specialized RobotDefinition')

    def set_kinematic_parameters(self, new_kinematic_parameters):
        raise NotImplementedError(
            'Must be overridden in specialized RobotDefinition')

    kinematic_parameters = property(get_kinematic_parameters,
                                    set_kinematic_parameters)

    def get_kinematic_jacobian(self):
        raise NotImplementedError(
            'Must be overridden in specialized RobotDefinition')

    kinematic_jacobian = property(get_kinematic_jacobian)

    def get_dof(self):
        """Return the number of Degrees of Freedom."""
        return self._dof

    dof = property(get_dof)

    def get_frame_computer(self):
        return self._fc

    def set_frame_computer(self, new_frame_comp):
        self._frame_comp = new_frame_comp

    frame_computer = property(get_frame_computer, set_frame_computer)

    def get_link_xforms(self):
        """Return an ordered set of link transform."""
        return self._link_xforms

    link_xforms = property(get_link_xforms)

    def get_joint_xforms(self):
        """Return an ordered set of joint objects."""
        return self._joint_xforms

    joint_xforms = property(get_joint_xforms)

    def getJointXForms(self):
        """Return an ordered set of joint objects."""
        raise NotImplementedError(
            'Must be overridden in specialized RobotDefinition')

    def actuator2encoder(self, qact):
        """Compute vector of encoder positions given a actuator"""
        # Example (Simple, affine joint encoders):
        # return self.gains * qact + self.offs
        raise NotImplementedError(
            'Must be overridden in specialized RobotDefinition')

    def encoder2actuator(self, qenc):
        """Compute the actuator kinematics joint configuration from
        the encoder vector."""
        # Example (Simple, affine joint encoders):
        # return (qenc - self.offs) / self.gains
        raise NotImplementedError(
            'Must be overridden in specialized RobotDefinition')

    def actuator2serial(self, qact):
        """Convert serial kinematics configuration to actuator (actuator joint)
        kinematics."""
        # Example (Simple serial mechanism):
        # return qact
        raise NotImplementedError(
            'Must be overridden in specialized RobotDefinition')

    def serial2actuator(self, qser):
        """Convert serial kinematics configuration to actuator
        kinematics."""
        # Example (Simple serial mechanism):
        # return qser
        raise NotImplementedError(
            'Must be overridden in specialized RobotDefinition')

    def serial2encoder(self, qser):
        """Convert serial joint configuration to encoder vector."""
        # Example (Simple, affine joint encoders):
        # return self.gains * qser + self.offs
        raise NotImplementedError(
            'Must be overridden in specialized RobotDefinition')

    def encoder2serial(self, qenc):
        """Convert a encoder vector to serial joint configuration."""
        # Example (Simple, affine joint encoders):
        # return (qenc - self.offs) / self.gains
        raise NotImplementedError(
            'Must be overridden in specialized RobotDefinition')
