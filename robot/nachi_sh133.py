# coding=utf-8
"""
Module with definitions for the Nachi SH133 robot.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2013"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import numpy
import math3d

from .robotdefinition import RobotDefinition
from pymoco.kinematics import joints

class NachiSH133(RobotDefinition):

    def __init__(self, **kwargs):
        self._dof = 6
        RobotDefinition.__init__(self, **kwargs)

        # Home pose
        self._q_home = numpy.array([0.0, numpy.pi/2, 0.0, 0.0, 0.0, 0.0])

        # gains in encoder units per radian of actuator joint
        # configuration values: qEnc = gain*qAct+offset. Gain and
        # offset vector will depend on the calibration of the robot!
        self._nominal_encoder_gains = numpy.array(
            [-65299.6, 74968.3, -71207.4, 49451.2, 48643.5, -37524.6],
            dtype=numpy.double)
        self._encoder_gains =  kwargs.get(
            'encoder_gains', self._nominal_encoder_gains.copy())
        self._nominal_encoder_offsets = numpy.array(
            [0x80000, 0x80000, 0x80000, 0x80000, 0x80000, 0x80000],
            dtype=numpy.long)
        self.encoder_offsets = kwargs.get(
            'encoder_offsets', self._nominal_encoder_offsets.copy())
        # Std offset is probably special to the Nachi
        # controller. Added/subtracted after/before conversion,
        # directly in actuator space.
        self._std_act_offsets = numpy.array(
            [0.0, numpy.pi/2, 0.0, 0.0, 0.0, 0.0])

        # Joint position limits. Asymmetric for the Nachi SH133. Note
        # that the position limits are set according to the
        # specification, and then offset-corrected with the home
        # position.
        self._pos_lim_act = numpy.array(
            [[-3.14, -1.05, -2.38, -6.28, -2.36, -6.28],
             [ 3.14,   1.4  ,  4.19,   6.28,  2.36,  6.28]], dtype=numpy.double)
        self._pos_lim_act += self._q_home
        self._pos_lim_enc= ((self._encoder_gains 
                             * self._pos_lim_act).astype(numpy.long) 
                            + self._encoder_offsets)
        self._pos_lim_enc.sort(axis=0)
        #pos_lim_actLower, pos_lim_actUpper = pos_lim_act
        #pos_lim_encLower, pos_lim_encUpper = pos_lim_enc

        # Joint speed limits. Symmetric
        self._spd_lim_act = numpy.array(
            [1.75, 1.57, 1.66, 3.94, 3.94, 5.32], dtype=numpy.double)
        self._spd_lim_enc = (abs(self._encoder_gains) *
                             self._spd_lim_act).astype(numpy.long)

        # The llc sends 6 parallel joint angles (floats in radians)
        # commanded from the main controller, and 6 parallel joint angles
        # (floats in radians) commanded from the OLIMEX card to the llc.
        # llcOutStructEnc = struct.Struct('<6L6L')
        # llcOutStructAct = struct.Struct('<6d6d')

        # The llc reads 6 parallel joint angles (floats in radians).
        # llcInStructEnc = struct.Struct('<6L')
        # llcInStructAct = struct.Struct('<6d')

        # Flag for sending of angles; otherwise OLIMEX encoder package
        # anglesIO = False

        # Internal link transform for the seven robot parts.
        self._link_xforms = [math3d.Transform() for i in range(7)]

        self._link_xforms[0]._v.z = 0.312

        self._link_xforms[1]._o.rotateX(numpy.pi / 2)
        self._link_xforms[1]._v.x = 0.305
        self._link_xforms[1]._v.y = 0.062
        self._link_xforms[1]._v.z = 0.77 - self._link_xforms[0]._v.z

        self._link_xforms[2]._v.x = 1.11
        self._link_xforms[2]._v.z = -0.067


        self._link_xforms[3]._o.rotateX(numpy.pi / 2)
        self._link_xforms[3]._v.x = 0.2
        self._link_xforms[3]._v.y = -1.035
        self._link_xforms[3]._v.z = 0.129

        self._link_xforms[4]._o.rotateX(-numpy.pi / 2)
        self._link_xforms[4]._v.z = 0.205 # 1.24 + _link_xforms[3]._v.y

        self._link_xforms[5]._o.rotateX(numpy.pi / 2)
        self._link_xforms[5]._v.y =  -0.206

        # Tool Mount transform
        self._link_xforms[6]._v.z = 0.02 # 0.225 + _link_xforms[5]._v.y

    def get_encoder_offsets(self):
        return self._encoder_offsets
    encoder_offsets = property(get_encoder_offsets)

    def get_joint_xforms(self):
        return [joints.RevoluteJoint() for i in range(6)]

    def getJointXForms(self):
        print('NachiSH133.getJointXForms : Deprecated, use "get_joint_xforms".')
        return [joints.RevoluteJoint() for i in range(6)]

    def actuator2encoder(self, qact):
        """ Compute the encoder vector given a actuator"""
        return (self._encoder_gains 
                * (qact - self._std_act_offsets)
                ).astype(numpy.long) + self._encoder_offsets

    def encoder2actuator(self, qenc):
        """ Compute the actuator kinematics joint configuration from
        the encoder vector.""" 
        return ((qenc - self._encoder_offsets) / self._encoder_gains 
                + self._std_act_offsets)

    def actuator2serial(self, qact):
        """ Convert serial kinematics configuration to actuator
        (actuatoruator joint) kinematics. Trivial due to identity of
        actuator and serial kinematics."""
        return qact

    def serial2actuator(self, qser):
        """ Convert serial kinematics configuration to actuator
        kinematics. Trivial due to identity of actuator and serial
        kinematics."""
        return qser

    def serial2encoder(self, qser):
        """ Convert serial joint configuration to encoder vector."""
        return (self._encoder_gains * (qser - self._std_act_offsets)
                ).astype(numpy.long) + self._encoder_offsets

    def encoder2serial(self, qenc):
        """ Convert a encoder vector to serial joint configuration."""
        return ((qenc - self._encoder_offsets) / self._encoder_gains 
                + self._std_act_offsets)
