# coding=utf-8
"""
Module with definitions for the Universal Robots UR5 with
implementation of special ML-kinematics.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012-2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import struct

import numpy as np
import math3d as m3d

#from ..pymoco
from pymoco.kinematics import joints
from .ur_base import UR_Base

class UR5_ML(UR_Base):

    def __init__(self, **kwargs):
        UR_Base.__init__(self, **kwargs)


        # Internal link transform for the seven robot parts.

        self._link_xforms = [m3d.Transform() for i in range(self._dof + 1)]

        self._link_xforms[0]._v.z = 0.021

        self._link_xforms[1]._o.rotate_xt(np.pi / 2)
        self._link_xforms[1]._v.y = -0.061
        self._link_xforms[1]._v.z = 0.0682

        self._link_xforms[2]._v.x = -0.425

        self._link_xforms[3]._v.x = -0.39243
        self._link_xforms[3]._v.z = 0.002750

        self._link_xforms[4]._o.rotate_xt(np.pi / 2)
        self._link_xforms[4]._v.y = -0.047750
        self._link_xforms[4]._v.z = 0.045250

        self._link_xforms[5]._o.rotate_xt(-np.pi / 2)
        self._link_xforms[5]._v.y = 0.049215
        self._link_xforms[5]._v.z = 0.045250

        self._link_xforms[6]._v.z = 0.032785

    def get_joint_xforms(self):
        return [joints.RevoluteJoint() for i in range(self._dof)]
    joint_xforms = property(get_joint_xforms)
