
__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2014"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import time
import math3d as m3d

_cycle = 0
_init_time = time.time()
_start_position = m3d.Vector(0.45106, -0.34465, -0.07262)
_tol = 0.01

def _tvc_rect(side_length=0.05, speed=0.01, cmd_freq=200.0):
    global _cycle, _init_time, _cm, _init_pose, _tol
    fc = _cm.robot_facade.frame_computer
    tvc = _cm.tvc()
    acc = [1,2]
    tvc._max_acc = acc
    cmd_period = 1.0 / cmd_freq
    tvc.timeout = 1.1 * cmd_period
    segment_time = side_length / speed
    while True:
        _cycle += 1
        t_segment_start = time.time()
        while segment_time > time.time() - t_segment_start:
            t_cmd_start = time.time()
            tvc.set_twist([+speed, 0, 0, 0, 0, 0], acc_limits=acc)
            time.sleep(cmd_period - (time.time() - t_cmd_start))
        t_segment_start = time.time()
        while segment_time > time.time() - t_segment_start:
            t_cmd_start = time.time()
            tvc.set_twist([0, +speed, 0, 0, 0, 0], acc_limits=acc)
            time.sleep(cmd_period - (time.time() - t_cmd_start))
        t_segment_start = time.time()
        while segment_time > time.time() - t_segment_start:
            t_cmd_start = time.time()
            tvc.set_twist([-speed, 0, 0, 0, 0, 0], acc_limits=acc)
            time.sleep(cmd_period - (time.time() - t_cmd_start))
        t_segment_start = time.time()
        while segment_time > time.time() - t_segment_start:
            t_cmd_start = time.time()
            tvc.set_twist([0, -speed, 0, 0, 0, 0], acc_limits=acc)
            time.sleep(cmd_period - (time.time() - t_cmd_start))
        err = _init_pose.pos.dist(fc.tool_pose.pos)
        print('Runtime: {:.0f}s -- Cycle number: {:d} -- Pos. error: {:.5f}m'
              .format(time.time()-_init_time, _cycle, err))
        if  err > _tol:
            break

def _goto_init_pose():
    global _cm, _init_pose, _start_position, _tol
    pos = _cm.robot_facade.frame_computer.tool_pose.pos
    if pos.dist(_start_position) > _tol:
        tlc = _cm.tlc()
        tlc.set_target_pose(_init_pose)
        tlc.wait_for_idle()
        
def long_run(cont_man):
    global _init_time, _cm, _init_pose, _start_position
    _cm = cont_man
    _init_time = time.time()
    fc = _cm.robot_facade.frame_computer
    _init_pose = fc.tool_pose
    _init_pose.pos = _start_position
    while True:
        print('**** RETURNING TO INIT POSE *****')
        _goto_init_pose()
        _tvc_rect(side_length=0.1, speed=0.1)
        
""" Usage:
import test_long_run
test_long_run.long_run(cm)
"""
