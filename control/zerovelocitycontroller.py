# coding=utf-8

"""
Module for implementing a motion controller which maintains the robot
in the current position. It is useful only to maintain the real-time
constraint of responding to robot status with some control command.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2014"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import threading

import numpy as np

from . import controller

class ZeroVelocityController(
    controller.Controller, threading.Thread):
    """ Class implementing a tool velocity controller."""

    def __init__(self, **kwargs):
        controller.Controller.__init__(self, **kwargs)
        threading.Thread.__init__(self)
        self.daemon = True
        self._dqZero = np.zeros(self._rob_fac.robot_definition.dof, dtype=float)
        self._stop_flag = False
        
    def wait_for_idle(self, timeout=None):
        """ The ZeroVelocityController is always idle."""
        return True

    def abort(self, wait=True):
        """Aborting is trivial, since there is no task, and the robot
        is at rest.
        """
        pass

    def run(self):
        """ Main loop of the controller thread: Wait for an LLC event
        and respond by sending a zero increment."""
        if self._start_running:
            self.resume()
            self._log('Started running', 4)
        else:
            self._log('Started suspended', 4)
        while not self._stop_flag:
            ## Receive LLC data
            self._llc_event.wait()
            if self._stop_flag:
                break
            self._llc_event.clear()
            self._rob_fac.cmd_joint_increment = self._dqZero
        self._exit_event.set()
        self.suspend()

    def stop(self):
        """ Stop all thread activity."""
        self._stop_flag = True
        # Make sure to wake up
        self._llc_event.set()
        # Wait for control completion
        self._exit_event.wait()


