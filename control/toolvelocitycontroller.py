# coding=utf-8

"""
Module for implementing a tool velocity motion controller.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2011-2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import threading
import time
import gc

import numpy as np
import math3d as m3d
import math3d.dynamics

from .kinematicscontroller import KinematicsController


class ToolVelocityController(KinematicsController, threading.Thread):
    """Class implementing a tool velocity controller."""

    class Error(Exception):
        def __init__(self, message):
            Exception.__init__(self)
            self.message = message

        def __repr__(self):
            return self.__class__.__name__ + ' : ' + self.message

    def __init__(self, **kwargs):
        """Create a tool velocity controller. Supported 'kwargs' are
        'timeout' (0.2), 'eigen_eps' (1.0e-7).
        """
        threading.Thread.__init__(self)
        self.daemon = True
        KinematicsController.__init__(self, **kwargs)
        self._jit_jac = kwargs.get('jit_jacobian', True)
        self._timeout = kwargs.get('timeout', 0.2)
        self._eigen_eps = kwargs.get('eigen_eps', 1.0e-7)
        self._stop_flag = False
        self._twist_lock = threading.RLock()
        # NB! The twists are stored represented in the frame to which it is
        # attached
        self._target_twist = m3d.dynamics.OrigoTwist(np.zeros(6))
        self._tracked_twist = m3d.dynamics.OrigoTwist(np.zeros(6))
        self._last_command_time = 0.0
        # May be 'Tool' or 'Base'. Initially set to 'Base'
        self._attach = 'Base'
        self._express = 'Base'
        self._joint_pos = None
        # self._tool_orient = np.identity(3)
        self._idle_flag = threading.Event()
        self._idle_flag.set()
        self._timeout_flag = threading.Event()
        self._timeout_flag.set()
        self._max_acc = [5.0, 10.0]
        # Initialize internal variables to defaults with zero-twist
        self.set_twist(self._target_twist)

    def get_is_idle(self):
        """Query the idle-state of the controller. The controller is
        idle when the target twist has been achieved.
        """
        return not self._idle_flag.is_set()
    is_idle = property(get_is_idle)

    def wait_for_idle(self, timeout=None):
        """Block until the idle flag is set, signalling that the
        controller has achieved the target twist.
        """
        if timeout is None:
            self._idle_flag.wait()
        else:
            self._idle_flag.wait(timeout)
        return self._idle_flag.is_set()

    def abort(self, wait=True):
        """Abort the twist task currently running. This is achieved
        by manipulating last command time, whereby braking is triggered.
        """
        # Make the target 0
        self.twist = (0, 0, 0, 0, 0, 0)
        # Trigger timeout-braking
        self._last_command_time = time.time() - self._timeout - 1
        if wait:
            while True:
                self._rob_fac.wait_for_control()
                if (np.linalg.norm(self._rob_fac.cmd_joint_increment) /
                    self._rob_fac.control_period) < 1.e-3:
                    break

    def get_timeout(self):
        """Return the timeout period, in which the controller will
        continue without receiving an updated command.
        """
        return self._timeout

    def set_timeout(self, new_timeout):
        """Set the timeout."""
        self._timeout = float(new_timeout)

    timeout = property(get_timeout, set_timeout)

    def wait_for_timeout(self, timeout=None):
        """Block until the timeout flag is set."""
        if timeout is None:
            self._timeout_flag.wait()
        else:
            self._timeout_flag.wait(timeout)
        return self._timeout_flag.is_set()

    def get_is_timedout(self):
        """Query the status of the timeout flag."""
        return self._timeout_flag.is_set()

    is_timedout = property(get_is_timedout)

    def set_twist(self, twist, axis_point=m3d.Vector(),
                  attach_frame='Base', express_frame='Base',
                  acc_limits=(4.0, 6.0)):
        """Set the 'twist', considered fixed in the frame denoted by
        'attach_frame', and expressed in the frame denoted by
        'express_frame'. A twist is composed of a linear and an
        angular velocity combined to six numbers in that
        order. 'twist' may be given in any type over which an
        m3d.dynamics.OrigoTwist can be constructed. The 'axis_point'
        will be considered a point on the axis of rotation for the
        twist. 'axis_point' must be a point given in tool coordinates,
        and will be attached to the tool. 'acc_limits' is a pair of
        numbers used to cap the instantaneous accelerations in linear
        and angular parts of se(3) at any time. If set to 'None' there
        will be no capping of the instantaneous acceleration.
        """
        # if len(twist) != 6:
        #     raise ToolVelocityController.Error(
        #         'An OrigoTwist must be of length 6.')
        # if type(twist) != np.ndarray:
        #     # Make sure the representation is np.array
        #     twist = np.array(twist)
        # Make sure we have an OrigoTwist
        twist = m3d.dynamics.OrigoTwist(twist)
        if express_frame == 'Tool' and attach_frame == 'Base':
            # Rotate the twist to Base frame, where it is fixed
            twist = self._frame_comp().orient * twist
        elif express_frame == 'Base' and attach_frame == 'Tool':
            # Rotate the twist to Tool frame, where it is fixed
            twist = self._frame_comp().orient.inverse * twist
        with self._twist_lock:
            self._idle_flag.clear()
            self._axis_point = axis_point
            self._apx = m3d.Transform(m3d.Orientation(), self._axis_point)
            self._apx_inv = self._apx.inverse
            self._target_twist = twist
            self._express = express_frame
            self._attach = attach_frame
            self._acc_limits = acc_limits
            # self._acc_clip_low[:3].fill(-self._acc_limits[0])
            # self._acc_clip_low[3:].fill(-self._acc_limits[1])
            # self._acc_clip_high[:3].fill(self._acc_limits[0])
            # self._acc_clip_high[3:].fill(self._acc_limits[1])
        self._last_command_time = time.time()
        self._timeout_flag.clear()

    def get_current_twist(self):
        """Return the tracked twist, i.e. the twist which is currently
        applied to the robot tool.
        """
        return m3d.dynamics.OrigoTwist(self._tracked_twist)

    current_twist = property(get_current_twist)

    def get_target_twist(self):
        """Return the target twist, towards which the controller is
        currently moving the applied twist.
        """
        return m3d.dynamics.OrigoTwist(self._target_twist)

    def update_target_twist(self, twist=None):
        """Update only the given target twist, not the control
        conditions and settings. In contrast to 'set_twist', no change
        to any parameters controlling the interpretation and obtaining
        the twist is changed. If 'twist' is None, this method merely
        function as a keep-alive to avoid timeout."""
        if twist is not None:
            twist = m3d.dynamics.OrigoTwist(twist)
            if self._express == 'Tool' and self._attach == 'Base':
                # Rotate the twist to Base frame, where it is fixed
                twist = self._frame_comp().orient * twist
            elif self._express == 'Base' and self._attach == 'Tool':
                # Rotate the twist to Tool frame, where it is fixed
                twist = self._frame_comp().orient.inverse * twist
            with self._twist_lock:
                self._target_twist = twist
        self._last_command_time = time.time()
        self._timeout_flag.clear()

    target_twist = property(get_target_twist, update_target_twist)

    def _update_tracking(self):
        """Update the tracked twist towards the target, while
        respecting tool acceleration limits.
        """
        with self._twist_lock:
            twist_error = self._target_twist - self._tracked_twist
            # Check if the target twist have been achieved
            if (twist_error.linear.length < 1e-4
                and twist_error.angular.length < 1e-4):
                self._idle_flag.set()
            # Update the linear part to obey acceleration limits
            if not self._acc_limits[0] is None:
                delta_twist_lin = twist_error.linear
                if delta_twist_lin.length < 1e-6:
                    delta_lin_factor = 999.0
                else:
                    delta_lin_factor = (
                        self._acc_limits[0] *
                        self._rob_fac.control_period /
                        delta_twist_lin.length)
                if delta_lin_factor < 1:
                    delta_twist_lin *= delta_lin_factor
                self._tracked_twist.linear += delta_twist_lin
            # Update the angular part to obey acceleration limits
            if not self._acc_limits[1] is None:
                delta_twist_ang = twist_error.angular
                if delta_twist_ang.length < 1e-6:
                    delta_ang_factor = 999.0
                else:
                    delta_ang_factor = (
                        self._acc_limits[1] *
                        self._rob_fac.control_period / delta_twist_ang.length)
                if delta_ang_factor < 1:
                    delta_twist_ang *= delta_ang_factor
                self._tracked_twist.angular += delta_twist_ang


    def _update_ij(self):
        """Update the invers Jacobian."""
        with self._llc_lock:
            q_cmd = self._rob_fac.cmd_joint_pos
        self._ij = self._frame_comp.inverse_jacobian(
            q_cmd, self._eigen_eps)

    def _apply_twist(self):
        """Apply the tracked twist to the current motion of the
        tool.
        """
        if self._jit_jac:
            # Calculate the inverse Jacobian just in time
            self._update_ij()
        tool_pose_b = self._frame_comp.tool_pose
        tool_orient_b = tool_pose_b.orient
        with self._twist_lock:
            # Update the tracked twist
            self._update_tracking()
            # Sample the current twist, assumed in base
            twist = m3d.dynamics.OrigoTwist(self._tracked_twist)
        if self._attach == 'Base':
            # Transform the twist to tool orientation
            twist = tool_orient_b.inverse * twist
        # Calculate the twist displacement with the given axis point
        # Get the displacement by applying the twists for one control period.
        displ = twist.displacement(self._rob_fac.control_period)
        # Transport the displacement to the tool coordinate system,
        # and express it in base orientation.
        d_tool = (tool_orient_b *
                  (self._apx * displ * self._apx_inv).pose_vector)
        return np.dot(self._ij, d_tool)

    def llc_notify(self, event_time):
        """Custom handler for LLC notification overriding the one in
        the Controller base class. Here garbage collection is disabled
        and must be enabled again by the response handler.
        """
        gc.disable()
        with self._llc_lock:
            self._llc_notify_time = event_time
            self._llc_event.set()

    def _timeout_brake(self):
        """On timeout, braking is achieved by setting a zero twist
        target with '_max_acc' acceleration."""
        with self._twist_lock:
            self._target_twist = m3d.dynamics.OrigoTwist(np.zeros(6))
            self._acc_limits = self._max_acc

    def run(self):
        """Main loop of the controller thread: Wait for an LLC event,
        compute the motion based on the current twist, and send
        through the base controller.
        """
        # Initialize the inverse Jacobian
        self._update_ij()
        self._lateness = 0.0
        if self._start_running:
            self._log('Started running', 4)
            self.resume()
        else:
            self._log('Started suspended', 4)
        while not self._stop_flag:
            # Receive LLC data
            self._llc_event.wait()
            if self._stop_flag:
                break
            # // Disabled in reception of llc event.
            # gc.disable()
            t_start = time.time()
            self._llc_event.clear()
            dtime = time.time()-self._rob_fac.current_arrival_time
            if dtime > 0.005:
                if self._log_level >= 1:
                    self._log('Control computation starting more than ' +
                              '5ms (%.5fs) after event time!'.format(dtime), 1)
            dq = self._apply_twist()
            if self._log_level >= 5:
                self._log('Sending control step {}'.format(dq), 5)
            self._rob_fac.cmd_joint_increment = dq
            self._lateness = (0.001 * self._lateness
                              + 0.999 * (time.time()-self._rob_fac.current_arrival_time))
            if (not self._timeout_flag.is_set() 
                and t_start - self._last_command_time > self._timeout):
                self._timeout_flag.set()
                if self._log_level >= 4:
                    self._log('Watchdog timeout at %#.2f' % time.time(), 4)
                self._timeout_brake()
                #self._rob_fac.cmd_joint_increment = self._dq_zero
            if not self._jit_jac:
                # Pre-calculate the inverse Jacobian for next cycle
                self._update_ij()
            if self._llc_event.is_set():
                if self._log_level >= 1:
                    self._log('Error : LLC control request came '
                              + 'during control computation! {}'
                              .format(self._rob_fac.cycle_number), 1)
            gc.enable()
        self._exit_event.set()
        self.suspend()

    def stop(self):
        """ Stop all thread activity."""
        self._stop_flag = True
        # Ensure no further motion
        self._timeout_flag.set()
        # Make sure to wake up
        self._llc_event.set()
        # Wait for control completion
        self._exit_event.wait()
