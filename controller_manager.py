#!/usr/bin/env python3
# coding=utf-8
"""
Controller manager module. A controller handles the interfacing to a
robot controller, and manages motion controllers working with that
robot.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import os
import sys
import socket
import time
import copy
import struct
import traceback
import threading

import numpy as np
import math3d as m3d

from pymoco.control.jointlinearcontroller import JointLinearController
from pymoco.control.jointvelocitycontroller import JointVelocityController
from pymoco.control.toollinearcontroller import ToolLinearController
from pymoco.control.toolcorrectioncontroller import ToolCorrectionController
from pymoco.control.pathcorrectioncontroller import PathCorrectionController
from pymoco.control.toolvelocitycontroller import ToolVelocityController
from pymoco.control.zerovelocitycontroller import ZeroVelocityController
import pymoco.facades

LOG_LEVEL = 2


def setup_test_transforms():
    """ Generate a set of test transforms to use as linear move
    targets."""

    global tleft, tright, tback, thome, q_home, t0, t1

    toolXForm = m3d.Transform()
    toolXForm.orient.rotateYT(np.pi / 2)
    toolXForm.pos.x = 0.15

    q_home = con_man.robot_facade.robot_definition.q_home
    thome = con_man.robot_facade.frame_computer(q_home)
    t = con_man.robot_facade.frame_computer(q_home)
    t.pos += m3d.Vector(-0.5, 0.5, 0.25)
    t.orient.rotate_yt(np.pi / 6)

    tleft = copy.copy(thome)
    tright = copy.copy(thome)

    tleft.pos += m3d.Vector(-0.2, 0.2, -0.2)
    tleft.orient.rotate_yt(-np.pi / 6)
    tleft.orient.rotate_zt(np.pi / 6)
    tright.pos += m3d.Vector(-0.2, -0.2, -0.2)
    tright.orient.rotate_yt(np.pi / 6)
    tright.orient.rotate_zt(-np.pi / 6)

    tback = copy.copy(tleft)
    tback.pos.x -= .7
    tback.pos.y += 0.5
    tback.orient.rotate_zb(np.pi / 2)
    tback.orient.rotate_zt(np.pi / 2)

    # o = m3d.Orientation()
    # o.rotateYT(np.pi / 2)
    # v = m3d.Vector(1, 0, 1)
    # toolRobotHomePose = m3d.Transform(o, v)

    # o = m3d.Orientation()
    # o.rotateYT(np.pi)
    # v = m3d.Vector(1, 0, 1.1)
    # toolTableHomePose = m3d.Transform(o, v)

    t0 = thome.copy()
    t0.pos.y += 0.3
    t0.pos.z -= 0.3
    t1 = thome.copy()
    t1.pos.y -= 0.5
    t1.pos.z -= 0.3


def log(msg, log_level=LOG_LEVEL):
    global LOG_LEVEL
    if log_level <= LOG_LEVEL:
        print(str(log_level) + ' : controller_manager::'
              + traceback.extract_stack()[-2][2] + ' : ' + msg)


class ControllerManager(object):
    """ Basic management of motion generators to ensure real-time
    constraints and policies for switching controllers."""
    def __init__(self,
                 rob_type='ur',
                 rob_facade=None,
                 rob_host='127.0.0.1',
                 rob_out_port=5556,
                 rob_in_port=5555,
                 rob_port=5002,
                 rob_sock_type='TCP',
                 controller='',
                 log_level=2,
                 **kwargs):
        global pymoco
        self._cm_lock = threading.Lock()
        self._log_level = log_level
        kwargs['log_level'] = log_level
        self._controller = None
        if rob_type in ['ur', 'UR5']:
            log('Selecting Universal Robots robot with '
                + '%s-based router connection' % rob_sock_type, LOG_LEVEL)
            kwargs['gw_host'] = rob_host
            if rob_sock_type == 'TCP':
                kwargs['gw_port'] = rob_port
                self._rob_fac = pymoco.facades.URTCPLegacyFacade(
                    **kwargs)
            elif rob_sock_type == 'UDP':
                kwargs['gw_in_port'] = rob_in_port
                kwargs['gw_out_port'] = rob_out_port
                self._rob_fac = pymoco.facades.URUDPLegacyFacade(
                    **kwargs)
            else:
                raise Exception('self._rob_fac_factory: '
                                + '"rob_sock_type" must be in ["UDP", "TCP"]')
        elif rob_type in ['sc15f', 'NachiSC15F']:
            log('Selecting Nachi SC15F robot', LOG_LEVEL)
            kwargs['rob_type'] = 'NachiSC15F'
            self._rob_fac = pymoco.facades.NachiOlimexLegacyFacade(
                olimex_host=rob_host,
                olimex_in_port=rob_in_port,
                olimex_out_port=rob_out_port,
                **kwargs)
        elif rob_type in ['sh133', 'NachiSH133']:
            log('Selecting Nachi SH133 robot', LOG_LEVEL)
            kwargs['rob_type'] = 'NachiSH133'
            self._rob_fac = pymoco.facades.NachiOlimexLegacyFacade(
                olimex_host=rob_host,
                olimex_in_port=rob_in_port,
                olimex_out_port=rob_out_port,
                **kwargs)
        elif rob_type in ['mc70', 'NachiMC70']:
            log('Selecting Nachi MC70 robot', LOG_LEVEL)
            kwargs['rob_type'] = 'NachiMC70'
            self._rob_fac = pymoco.facades.NachiOlimexLegacyFacade(
                olimex_host=rob_host,
                olimex_in_port=rob_in_port,
                olimex_out_port=rob_out_port,
                **kwargs)
        elif rob_type in ['Emika']:
            log('Selecting Franka Emika Robot', LOG_LEVEL)
            import pymoco.facades.franka
            self._rob_fac = pymoco.facades.franka.FMSFacade(fms_host=rob_host, fms_port=rob_port)
        else:
            raise Exception('Needs a recognized robot')
        # Some facades need an active controller before connecting (starting)!
        if controller != '':
            log('Starting "%s" controller' % controller, LOG_LEVEL)
            exec('self.%s()' % controller)
        else:
            log('Starting default Zero Velocity Controller.', LOG_LEVEL)
            self.zvc()
        log('Starting robot facade', LOG_LEVEL)
        self._rob_fac.start()
        self._stopped = False
        # // Give some time for the robot facade to start
        time.sleep(0.1)

    def get_robot_facade(self):
        """ Give direct access to the underlying robot system
        interface. This gives an umnanaged access to the underlying
        kinematics systems that may severey affect real-time operation
        of certain controllers. Use with care."""
        return self._rob_fac
    robot_facade = property(get_robot_facade)

    def get_controller_type_name(self):
        """ Return the name of the type (or class) of the running
        controller."""
        return self._controller.__class__.__name__

    controller_type_name = property(get_controller_type_name)

    def get_current_controller(self):
        """ Give access to the active controller."""
        return self._controller

    current_controller = property(get_current_controller)

    def get_tool_xform(self):
        """ Return the tool transform used in the kimematics
        computations. The tool transform is the homogeneous, static
        transform from operation (tool) coordinates to the robot tool
        flange."""
        return self._rob_fac.frame_computer.tool_xform

    def set_tool_xform(self, new_tool_xform):
        """ Set the specific tool transform used in the kimematics
        computations. This is done by a managed switch to the zero
        velocity controller."""
        # if type(self._controller) == ZeroVelocityController:
        #     self._rob_fac.frame_computer.tool_xform = new_tool_xform
        # else:
        #     log('Warning: Tried to set tool transform while controller operating.', 1)
        with self._cm_lock:
            self._controller.wait_for_idle()
            self._rob_fac.frame_computer.tool_xform = new_tool_xform
            # if type(self._controller) != ZeroVelocityController:
            #     new_controller = ZeroVelocityController(robot_facade=self._rob_fac, start_running=False)
            #     new_controller.start()
            #     if not self._controller is None:
            #         self._controller.wait_for_idle()
            #         self._controller.stop()
            #     new_controller.resume()
            #     self._controller = new_controller
            #     self._rob_fac.frame_computer.tool_xform = new_tool_xform

    tool_xform = property(get_tool_xform, set_tool_xform)

    def _start_controller(self, controller_class, **kwargs):
        with self._cm_lock:
            if type(self._controller) != controller_class or len(kwargs) > 0:
                if 'log_level' not in kwargs:
                    kwargs['log_level'] = self._log_level
                new_controller = controller_class(robot_facade=self._rob_fac,
                                                  start_running=False,
                                                  **kwargs)
                new_controller.start()
                if self._controller is not None:
                    # Wait for task-oriented controllers to finish
                    self._controller.wait_for_idle()
                    # Command continuous controllers to stop motion
                    self._controller.abort()
                    # Wait for end of a control cycle
                    self._rob_fac.wait_for_control()
                    # Suspend the current, and resume the new controller
                    self._controller.suspend()
                    new_controller.resume()
                    # Take down the old controller.
                    self._controller.stop()
                else:
                    new_controller.resume()
                self._controller = new_controller

    def jlc(self, **kwargs):
        self._start_controller(JointLinearController, **kwargs)
        return self._controller

    def jvc(self, **kwargs):
        self._start_controller(JointVelocityController, **kwargs)
        return self._controller

    def tlc(self, **kwargs):
        self._start_controller(ToolLinearController, **kwargs)
        return self._controller

    def tcc(self, **kwargs):
        self._start_controller(ToolCorrectionController, **kwargs)
        return self._controller

    def pcc(self, **kwargs):
        self._start_controller(PathCorrectionController, **kwargs)
        return self._controller

    def tvc(self, **kwargs):
        self._start_controller(ToolVelocityController, **kwargs)
        return self._controller

    def zvc(self, **kwargs):
        self._start_controller(ZeroVelocityController, **kwargs)
        return self._controller

    def stop(self, join=False):
        if not self._stopped:
            self._stopped = True
            self._controller.stop()
            self._rob_fac.stop(join)
            if join:
                self._controller.join()

# Backward compatibility:


if __name__ == '__main__':
    """ Create a default controller manager, and set up for invoking
    like in client_utils."""
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('rob_type',
                        type=str, choices=pymoco.robot.get_robot_types(),
                        default='')
    parser.add_argument('--controller',
                        type=str,
                        choices=[
                            'tlc', 'tvc', 'jlc', 'jvc', 'zvc'], default='zvc')
    parser.add_argument('--rob_host', type=str, default='127.0.0.1')
    parser.add_argument('--rob_out_port',
                        type=int, default=pymoco.input.llc_out_port)
    parser.add_argument('--rob_in_port',
                        type=int, default=pymoco.control.llc_in_port)
    parser.add_argument('--rob_port', type=int, default=5002)
    parser.add_argument('--rob_sock_type',
                        type=str, choices=['UDP', 'TCP'], default='TCP')
    parser.add_argument('--bind_host', type=str, default='0.0.0.0')
    parser.add_argument('--encoder_offsets',
                        type=str, default='',
                        help="""File which holds the encoder offsets for the specific robot in
                        .npy- or .npy.txt-format."""
                        )
    parser.add_argument('--log_level', type=int, default=2)
    args = parser.parse_args()
    LOG_LEVEL = args.log_level
    if args.rob_type is not '':
        if os.path.isfile(args.encoder_offsets):
            if os.path.splitext(args.encoder_offsets)[1] == '.txt':
                args.encoder_offsets = np.loadtxt(args.encoder_offsets)
            elif os.path.splitext(args.encoder_offsets)[1] == '.npy':
                args.encoder_offsets = np.load(args.encoder_offsets)
        else:
            del args.encoder_offsets
        con_man = ControllerManager(**vars(args))
        cm = con_man
        robot_facade = cm.robot_facade
        # setup_test_transforms()
        # starttvc = con_man.tvc
        # starttlc = con_man.tlc
        import atexit
        atexit.register(con_man.stop)
    # import code
    # code.interact('', None, globals())
